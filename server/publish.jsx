TasksCollections = new Mongo.Collection("tasks");
ProjectsCollections = new Mongo.Collection("projects");
Customers = new Mongo.Collection("customers");
ProjectDetails = new Mongo.Collection("details");
Notifications = new Mongo.Collection("notifications");
Profiles = new Mongo.Collection("profiles");
TasksArchive = new Mongo.Collection("archivedTasks");
ProjectsArchive = new Mongo.Collection("archivedProjects");

Meteor.publish("listCustomers", function() {
    return Customers.find({$or: [{user: this.userId}, {assigned: this.userId}]});
});

Meteor.publish("listProjects", function() {
    return ProjectsCollections.find({$or: [{user: this.userId}, {assigned: this.userId}]},{sort: {startDate: +1}});
});

Meteor.publish("listProfiles", function() {
    return Profiles.find();
});

Meteor.publish("listTasksArchive", function() {
    return TasksArchive.find({$or: [{user: this.userId}, {assigned: this.userId}]});
});

Meteor.publish("listProjectsArchive", function() {
    return ProjectsArchive.find({$or: [{user: this.userId}, {assigned: this.userId}]});
});

Meteor.publish("taskList", function() {
    return TasksCollections.find();
});

Meteor.publish("projDetails", function() {
    return ProjectDetails.find();
});

Meteor.publish("notifications", function() {
    return Notifications.find({$or: [{user: this.userId}, {assigned: this.userId}]});
});
