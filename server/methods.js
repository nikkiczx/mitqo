Meteor.methods({
    'Accounts.config': function( options ) {
      if ( __meteor_runtime_config__.accountsConfigCalled ) {
          throw new Meteor.Error( 500,
              'Options already set',
              'Options already set [500]'
          );
      }

      Accounts.config( options );
    },
    deleteTask(itemID) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        TasksCollections.remove({_id: itemID});
    },
    deleteProject(itemID) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        ProjectsCollections.remove({_id: itemID});
    },
    toggleLockUnlockProject(projectID, dispStatus) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        ProjectsCollections.update(
            {_id: projectID},
            {$set: {disp: dispStatus}}
        );
    },
    updateProgress(taskID, end, progressStatus, update) {
      if (!Meteor.userId()) {
          throw new Meteor.Error('not-authorized');
      }
      TasksCollections.update(
          {_id: taskID},
          {$set: {endDate: end, progress: progressStatus, comment: update}}
      );
    },
    updateProject(projectID, end, progressStatus) {
      if (!Meteor.userId()) {
          throw new Meteor.Error('not-authorized');
      }
      ProjectsCollections.update(
          {_id: projectID},
          {$set: {endDate: end, progress: progressStatus}}
      );
    },
    editTask(taskID, task, start, end, assigned) {
      if (!Meteor.userId()) {
          throw new Meteor.Error('not-authorized');
      }
      TasksCollections.update(
          {_id: taskID},
          {$set: {taskName: task, startDate: start, endDate: end, assigned: assigned}}
      );
    },
    archiveProject(projID, custId, custName, project, start, end, progress, assigned) {
        check(assigned, String);
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        ProjectsArchive.insert({
            customerID: custId,
            customerName: custName,
            projectID: projID,
            projectName: project,
            startDate: start,
            endDate: end,
            progress: progress,
            disp: "unlocked",
            archivedOn: new Date(),
            assigned: assigned,
            user: Meteor.userId()
        });
    },
    archiveTask(project, parentTask, subID, taskID, task, startDate, endDate, childLevel, assigned, progress) {
        check(task, String);
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
            TasksArchive.insert({
            projectID: project,
            parentID: parentTask,
            taskID: taskID,
            subID: subID,
            taskName: task,
            startDate: startDate,
            endDate: endDate,
            progress: progress,
            child: Number(childLevel),
            assigned: assigned,
            archivedOn: new Date(),
            user: Meteor.userId()
        });
    },
    addProjDetail(project, projID, task, details, customer, iconpic, assigned) {
        ProjectDetails.insert({
            user: Meteor.userId(),
            createdAt: new Date(),
            projectName: project,
            projectID: projID,
            taskID: task,
            detail: details,
            iconfig: iconpic,
            assigned: assigned,
            customerID: customer
        });
    },
    addTask: function(project, parentTask, subID, task, startDate, endDate, childLevel, assigned, user) {
        check(task, String);
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
            let id = TasksCollections.insert({
            projectID: project,
            parentID: parentTask,
            subID: subID,
            taskName: task,
            startDate: startDate,
            endDate: endDate,
            progress: '0',
            comment: "",
            child: Number(childLevel),
            assigned: assigned,
            createdAt: new Date(),
            user: user
        });
        return id;
    },
    updateTaskID(taskID) {
        TasksCollections.update(
          {_id: taskID},
          {$set: {parentID: taskID}}
        );
    },
    updateTaskSub(taskID, subID) {
        TasksCollections.update(
          {_id: taskID},
          {$set: {subID: subID}}
        );
    },
    updateTaskStart(taskID, start) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        TasksCollections.update(
            {_id: taskID},
            {$set: {startDate: start}}
        );
    },
    updateTaskEnd(taskID, end) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        TasksCollections.update(
            {_id: taskID},
            {$set: {endDate: end}}
        );
    },
    updateProjectStart(projectID, start) {
        if(!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        ProjectsCollections.update(
            {_id: projectID},
            {$set: {startDate: start}}
        );
    },
    updateProjectEnd(projectID, end) {
        if(!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        ProjectsCollections.update(
            {_id: projectID},
            {$set: {endDate: end}}
        );
    },
    editProject(projectID, project, customer, assigned) {
        check(assigned, String);
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        ProjectsCollections.update(
            {_id: projectID},
            {$set: {projectName: project, customerID: customer, assigned: assigned}}
        );
    },
    addCustomerAssistant(custId, assigned) {
        check(assigned, String);
        if (!Meteor.userId()) {
            throw new Meteor.error('not-authorized');
        }
        Customers.update(
          {_id: custId},
          {$set: {assigned: assigned}}
        );
    },
    addProject: function(custId, custName, project, start, end, assigned) {
        check(assigned, String);
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        let id = ProjectsCollections.insert({
            customerID: custId,
            customerName: custName,
            projectName: project,
            startDate: start,
            endDate: end,
            progress: '0',
            disp: "unlocked",
            createdAt: new Date(),
            assigned: assigned,
            user: Meteor.userId()
        });
        return id;
    },
    newProfile(FirstName, LastName, email) {
        check(email, String);
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        Profiles.insert({
            firstName: FirstName,
            lastName: LastName,
            email: email,
            assisting: [],
            assistedBy: [],
            updatedOn: new Date(),
            user: Meteor.userId(),
            access: "NA",
            status: "inactive"
        });
    },
    updateProfile(profileID, FirstName, LastName) {
        check(FirstName, String);
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        Profiles.update(
          {_id: profileID},
          {$set: {firstName: FirstName, lastName: LastName}}
        );
    },
    newCustomers(firstName, lastName, email, assign) {
        check(email, String);
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        Customers.insert({
            firstName: firstName,
            lastName: lastName,
            email: email,
            updatedOn: new Date(),
            user: Meteor.userId(),
            assigned: assign,
            status: "inactive"
        });
    },
    updateCustomers(customerID, firstName, lastName, assign) {
        check(lastName, String);
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        Customers.update(
            {_id: customerID},
            {$set: {firstName: firstName, lastName: lastName, assign: assign}}
        );
    },
    updateAssistant(profileID, assistantID) {
      if (!Meteor.userId()) {
          throw new Meteor.Error('not-authorized');
      }
      Profiles.update(
          {_id: profileID},
          {$push: {assistedBy: assistantID}}
      );
    },
    updateAssisting(assistingID) {
      if (!Meteor.userId()) {
          throw new Meteor.Error('not-authorized');
      }
      Profiles.update(
          {user: Meteor.userId()},
          {$push: {assisting: assistingID}}
      );
    },
    removeAssisting(assistingID) {
      if (!Meteor.userId()) {
          throw new Meteor.Error('not-authorized');
      }
      Profiles.update(
          {user: Meteor.userId()},
          {$pull: {assisting: assistingID}}
      );
    },
    removeAssistant(assistingID) {
      if (!Meteor.userId()) {
          throw new Meteor.Error('not-authorized');
      }
      Profiles.update(
          {user: assistingID},
          {$pull: {assistedBy: Meteor.userId()}}
      );
    },
    toggleResolution(resolution){
        if(Meteor.userId() !== resolution.user) {
            throw new Meteor.Error('not-authorized');
        }
        Resolutions.update(resolution._id, {
            $set: {complete : !resolution.complete}
        });
    },
    deleteResolution(resolution){
        if(Meteor.userId() !== resolution.user) {
            throw new Meteor.Error('not-authorized');
        }
        Resolutions.remove(resolution._id);
    },
    sendEmail: function(to, from, subject, name, favRes, bestFriend) {
      check([to, from, subject], [String]);
      this.unblock();

      SSR.compileTemplate('htmlEmail', Assets.getText('html-email.html'));

      var emailData = {
        name: name,
        favoriteRestaurant: favRes,
        bestFriend: bestFriend,
      };

      Email.send({
        to: to,
        from: from,
        subject: subject,
        html: SSR.render('htmlEmail', emailData),
      });
    }
});
