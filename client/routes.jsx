import React from 'react';
import {mount} from 'react-mounter';

import {MainLayout} from './layouts/MainLayout.jsx';
import {PublicLayout} from './layouts/PublicLayout.jsx';
import MainPage from './main/MainPage.jsx';
import SignIn from './main/SignIn.jsx';
import DashboardPage from './main/DashboardPage.jsx';
import ProjectsPage from './pm/ProjectsPage.jsx';
import ProfilePage from './main/ProfilePage.jsx';
import ProjHistory from './pm/ProjHistory.jsx';
import ProjDetails from './pm/ProjDetails.jsx';
import CallError from './pm/CallError.jsx';

FlowRouter.route('/',{
    action() {
        mount(PublicLayout, {
            content: (<MainPage />)
        }
    )}
});

FlowRouter.route('/dashboard',{
  action: function(params) {
    Tracker.autorun(function() {
      if (Meteor.userId()) {
            mount(MainLayout, {
                content: (<DashboardPage />)
            }
        )} else {
          mount(MainLayout, {
              content: (<SignIn />)
          }
        )}
    })
  }
});

FlowRouter.route('/projects', {
  action: function(params) {
    Tracker.autorun(function() {
      if (Meteor.userId()) {
            mount(MainLayout, {
                content: (<ProjectsPage />)
            }
        )} else {
          mount(MainLayout, {
              content: (<SignIn />)
          }
        )}
    })
  }
});

FlowRouter.route('/history', {
  action: function(params) {
    Tracker.autorun(function() {
      if (Meteor.userId()) {
            mount(MainLayout, {
                content: (<ProjHistory />)
            }
        )} else {
          mount(MainLayout, {
              content: (<SignIn />)
          }
        )}
    })
  }
});

FlowRouter.route('/details', {
  action: function(params) {
    Tracker.autorun(function() {
      if (Meteor.userId()) {
            mount(MainLayout, {
                content: (<ProjDetails />)
            }
        )} else {
          mount(MainLayout, {
              content: (<SignIn />)
          }
        )}
    })
  }
});

FlowRouter.route('/profile', {
  action: function(params) {
    Tracker.autorun(function() {
      if (Meteor.userId()) {
            mount(PublicLayout, {
                content: (<ProfilePage />)
            }
        )} else {
          mount(PublicLayout, {
              content: (<SignIn />)
          }
        )}
    })
  }
});

FlowRouter.route('/error', {
    action() {
        mount(PublicLayout, {
            content: (<CallError />)
        }
    )}
});
