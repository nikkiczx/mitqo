import React from 'react';
import moment from "moment";
import Rodal from 'rodal';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

import NotificationsUpdate from './NotificationsUpdate.jsx';

export default class Notifications extends TrackerReact(React.Component) {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            submitType: "cancel",
            subscription: {
                allDetails: Meteor.subscribe("projDetails"),
                customers: Meteor.subscribe("listCustomer"),
                VSprojects: Meteor.subscribe("listProjects"),
                allTasks: Meteor.subscribe("taskList")
            }
        };
    }

    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }

    cancelButton() {
        this.setState({submitType: "cancel"});
    }

    saveButton() {
        this.setState({submitType: "save"});
    }

    getProjectsOverdue() {
        var tempProjects = ProjectsCollections.find({$and: [{progress: {$ne: "deleted"}}, {endDate: {$gte: new Date()}}]}).fetch();
        return tempProjects;
    }

    getAssignedTasksDueSoon() {
        var dateToday = new Date();
        var dueSoonDate = new Date(moment().add(3, 'days'));

        var tempAry = []; var temp;
        var project = ProjectsCollections.find({$or: [{assigned: Meteor.userId()}, {user: Meteor.userId()}]}).fetch();

        var tempList = project.map( projs => {
            temp = TasksCollections.find({$and: [{$and: [{projectID: projs._id},{assigned: {$ne: Meteor.userId()}}]},{$and: [{$nor:[{progress: "deleted"}, {progress: "completed"} ]}, {$and: [{endDate: {$lte: dueSoonDate}}, {endDate: {$gt: dateToday}}]}]}]}, {sort: {endDate: +1}}).fetch();
            tempAry = temp;
            return tempAry;
        })

        return tempAry;
    }

    getAssignedTasksOverdue() {
        var temp; var tempAry = [];
        var project = ProjectsCollections.find({$or: [{assigned: Meteor.userId()}, {user: Meteor.userId()}]}).fetch();

        project.map( projs => {
            temp = TasksCollections.find({$and: [{$and:[{projectID: projs._id}, {assigned: {$ne: Meteor.userId()}}]},{$and: [{$nor: [{progress: "deleted"},{progress: "completed"}]},{endDate: {$lte: new Date()}}]}]}, {sort: {endDate: +1}}).fetch();
            tempAry = temp;
            return tempAry;
        })

        return tempAry;
    }

    getTasksDueSoon() {
        //find projects due soon

        var dateToday = new Date();
        var dueSoonDate = new Date(moment().add(3, 'days'));
        var tempTasks = TasksCollections.find({$and: [{$or: [{assigned: Meteor.userId()},{$and: [{assigned: ""},{user: Meteor.userId()}]}]},{$and: [{$nor:[{progress: "deleted"}, {progress: "completed"} ]}, {$and: [{endDate: {$lte: dueSoonDate}}, {endDate: {$gt: dateToday}}]}]}]}, {sort: {endDate: +1}}).fetch();
        return tempTasks;
    }

    getTasksOverdue() {
        var tempTasks = TasksCollections.find({$and: [{$or: [{assigned: Meteor.userId()},{$and: [{assigned: ""},{user: Meteor.userId()}]}]},{$and: [{$nor:[{progress: "deleted"}, {progress: "completed"} ]}, {endDate: {$lte: new Date()}}]}]}, {sort: {endDate: +1}}).fetch();
        return tempTasks;
    }

    taskCompleted() {
        if (this.state.submitType === "save") {
        }
    }

    render() {
        return (
            <div className="col-xs-12 zero-padding">
                <h2 id="projectsTitle" className="redColor ">URGENT</h2>
                <div className="col-xs-12 zero-padding" style={{top:'16px'}}>
                    <ul className="zero-padding">
                      {this.getTasksOverdue().map( task => {
                      return (
                          <div className="notification-box-task col-xs-12 notification-overdue table-fonts" key={task._id}>
                              <NotificationsUpdate task={task} />
                          </div>
                      )})}
                    </ul>
                    <ul className="zero-padding">
                      {this.getTasksDueSoon().map ( task => {
                      return (
                          <div className="notification-box-dueSoon col-xs-12 table-fonts" key={task._id}>
                              <NotificationsUpdate task={task} />
                          </div>
                      )})}
                    </ul>
                    <ul className="zero-padding">
                      {this.getAssignedTasksOverdue().map ( task => {
                      return (
                          <div className="notification-box-task notification-assigned-overdue col-xs-12 table-fonts" key={task._id}>
                              <NotificationsUpdate task={task} />
                          </div>
                      )})}
                    </ul>
                    <ul className="zero-padding">
                      {this.getAssignedTasksDueSoon().map ( task => {
                      return (
                          <div className="notification-box-assigned-dueSoon col-xs-12 table-fonts" key={task._id}>
                              <NotificationsUpdate task={task} />
                          </div>
                      )})}
                    </ul>
                </div>
            </div>
            )
        }
    }
