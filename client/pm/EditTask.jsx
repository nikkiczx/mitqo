import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import DateTimeField from 'react-bootstrap-datetimepicker';
import moment from 'moment';

export default class EditTask extends TrackerReact(React.Component) {

    constructor(){
        super();

        this.state = {
            submitType: "cancel",
            dateStart:"",
            dateEnd:"",
            dispStart:"",
            dispEnd:""
        }
    }

    handleChange(e) {
        e.preventDefault();
        this.state.value = e.target.value;
    }

    editTask(e) {
        if (this.state.submitType === "save") {
            let taskID = Session.get('taskID');
            var task = this.refs.taskdesc.value.trim();
            var startDate = this.state.dateStart;
            var endDate = this.state.dateEnd;

            var taskFetch = TasksCollections.find({_id: taskID}).fetch();
            var project = taskFetch[0].projectID;

            var projDetails = ProjectsCollections.findOne({_id: project});
            var projectName = projDetails.projectName;

            var allTasksStart = TasksCollections.find({projectID: project}, {sort: {startDate: +1}}).fetch();
            var firstTask = allTasksStart[0].startDate;

            var allTasksEnd = TasksCollections.find({projectID: project}, {sort: {endDate: -1}}).fetch();
            var lastTask = allTasksEnd[0].endDate;

            var projectFetch = ProjectsCollections.find({_id: project}).fetch();
            var projStatus = projectFetch[0].disp;

            var changes = "";

            var timeStart = ProjectsCollections.find({_id: project, startDate: {$lte: startDate}}).count();
            var timeEnd = ProjectsCollections.find({_id: project, endDate: {$gte: endDate}}).count();

            var assistant = Session.get("assistantName");
            var newAssistantID = assistant;
            var oriAssistantID = taskFetch[0].assigned;
            var oriAssistantName = Profiles.findOne({user: oriAssistantID});
            var oriAssistantFirstName = oriAssistantName.firstName;
            var oriAssistantLastName = oriAssistantName.lastName;
            var newAssistantFirstName; var newAssistantLastName;

            if (!assistant) {
                var assistantID = Profiles.findOne({user: Meteor.userId()});
                newAssistantID = assistantID.user;
            }

            if (projStatus === "locked") {
            if (timeStart === 0) {
                e.preventDefault();
                Bert.alert( 'Start time is before Project start time', 'danger', 'fixed-top');
            } else if (timeEnd === 0) {
                e.preventDefault();
                Bert.alert( 'End time is after Project end time', 'danger', 'fixed-top');
                } else if (nowDateTime < timeEnd) {
                    e.preventDefault();
                    Bert.alert('Task is already overdue', 'danger', 'fixed-top');
                }
            }
            else {
                if (!task) {
                    task = Session.get('taskName');
                } else {
                    changes = "Renamed task from " + Session.get('taskName') + " to " + task;
                }

                if (!startDate) {
                    startDate = moment(this.state.dispStart, "x").toDate();
                } else {
                    if (changes) {
                        changes = changes + ", Changed start date/time from " + moment(this.state.dispStart, "x").toDate() + " to " + startDate;
                    } else {
                        changes = "Changed start date/time from " + moment(this.state.dispStart, "x").toDate() + " to " + startDate;
                    }
                }

                if (!endDate) {
                    endDate = moment(this.state.dispEnd, "x").toDate();
                } else {
                    if (changes) {
                        changes = changes + ", Changed end date/time from " + moment(this.state.dispEnd,"x").toDate() + " to " + endDate;
                    } else {
                        changes = "Changed end date/time from " + moment(this.state.dispEnd, "x").toDate() + " to " + endDate;
                    }
                }

                if (newAssistantID !== oriAssistantID) {
                    var newAssistantFind = Profiles.findOne({user: newAssistantID});
                    newAssistantFirstName = newAssistantFind.firstName;
                    newAssistantLastName = newAssistantFind.lastName;
                    assistant = newAssistantID;
                    changes = "Reassigned task '" + task + "' from " + oriAssistantFirstName + " " + oriAssistantLastName + " to " + newAssistantFirstName + " " + newAssistantLastName;
                }

                if (startDate < firstTask) {
                    Meteor.call('updateProjectStart', project, startDate);
                } else {
                    Meteor.call('updateProjectStart', project, firstTask);
                }

                if (endDate > lastTask) {
                    Meteor.call('updateProjectEnd', project, endDate);
                } else {
                    Meteor.call('updateProjectEnd', project, lastTask);
                }

                Meteor.call('editTask', taskID, task, startDate, endDate, newAssistantID);
                Meteor.call('addProjDetail', projectName, project, taskID, changes, "NA", "fa fa-pencil-square-o fa-lg", assistant);
            }
        }
    }

    cancelButton() {
        this.setState({submitType: "cancel"});
    }

    saveButton() {
        this.setState({submitType: "save"});
    }

    getTask() {
        let tempObj = TasksCollections.find({_id: Session.get('taskID')}).fetch();

        let sDate = moment(tempObj[0].startDate).format('MM/DD/YY h:mm A');
        let eDate = moment(tempObj[0].endDate).format('MM/DD/YY h:mm A');
        this.state.dispStart = sDate;
        this.state.dispEnd = eDate;
    }

    handleChangeStart(newStart) {
        var newStartTemp = moment(newStart, "x").toDate();
        this.state.dateStart = newStartTemp;
    }

    handleChangeEnd(newEnd) {
        var newEndTemp = moment(newEnd, "x").toDate();
        this.state.dateEnd = newEndTemp;
    }

    getAssistants() {
        return Profiles.find({assisting: Meteor.userId()}, {sort: {firstName: +1}}).fetch();
    }

    assignTo(e) {
        e.preventDefault();
        let assistantID = e.target.value;
        let assignedTo = Profiles.findOne({_id: assistantID});
        let assistant = assignedTo.user;
        Session.set("assistantName", assistant);
    }

    render() {
        let taskName = Session.get('taskName');
        let tempObj = TasksCollections.find({_id: Session.get('taskID')}).fetch();

        if (tempObj.length > 0){
            let sDate = moment(tempObj[0].startDate, "x").toDate();
            let eDate = moment(tempObj[0].endDate, "x").toDate();
            this.state.dispStart = sDate;
            this.state.dispEnd = eDate;
        }

        let assistantList = this.getAssistants().map( (assistantNames) => {
            return <option ref="assistantID" key={assistantNames._id} assistantNames={assistantNames} value={assistantNames._id}> {assistantNames.firstName} {assistantNames.lastName}</option>
        })

        return (
            <div className="addTask">
                <form onSubmit={this.editTask.bind(this)}>
                    <div className="col-xs-12" style={{height:'85px'}}>
                        <div className="h3-modal text-center">Edit Task</div>
                        <div className="row">
                            <div className="col-xs-6" >
                                <input className="col-xs-12" placeholder={taskName} ref="taskdesc"></input>
                            </div>
                            <div className="col-xs-3 zero-padding">
                                <DateTimeField onChange={this.handleChangeStart.bind(this)} />
                            </div>
                            <div className="col-xs-3 zero-padding">
                                <DateTimeField onChange={this.handleChangeEnd.bind(this)} />
                            </div>
                        </div>

                    </div>
                    <div className="col-xs-12" style={{height:'40px'}}>
                        <label className="col-xs-2">Assign to:</label>
                        <select label="Me" className="col-xs-5" onChange={this.assignTo.bind(this)}>
                            <option>Me</option>
                            {assistantList}
                        </select>
                    </div>
                    <div className="col-xs-12 modal-save" style={{height:'10px'}}/>
                    <div className="row"/>
                    <div className="col-xs-12">
                        <button className="btn btn-warning col-xs-3 text-center" type="submit" onClick={this.saveButton.bind(this)}>SAVE</button>
                        <button className="btn btn-default col-xs-3 pull-right text-center" type="submit" onClick={this.cancelButton.bind(this)}>CANCEL</button>
                    </div>
                </form>
            </div>
        )
    }
}
