import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import moment from "moment";

export default class CallError extends TrackerReact(React.Component) {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                Bert.alert( 'Start time is earlier than Project start time', 'danger', 'fixed-top');
            </div>
        )
    }
}
