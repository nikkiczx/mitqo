import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import DateTimeField from 'react-bootstrap-datetimepicker';
import moment from 'moment';

export default class AddProject extends TrackerReact(React.Component) {

    constructor(){
        super();

        this.state = {
            submitType: "cancel",
            dateStart: moment().toDate(),
            dateEnd: moment().toDate(),
            subscription: {
                customers: Meteor.subscribe("listCustomer"),
                profiles: Meteor.subscribe("listProfiles")
            }
        }
    }

    componentWillUnmount() {
        this.state.subscription.customers.stop();
        this.state.subscription.profiles.stop();
    }

    handleChangeStart(newStart) {
        var newStartTemp = moment(newStart, "x").toDate();
        this.setState({dateStart: newStartTemp});
    }

    handleChangeEnd(newEnd) {
        var newEndTemp = moment(newEnd, "x").toDate();
        this.setState({dateEnd: newEndTemp});
    }


    getCustomers() {
        return Customers.find({$or: [{user: Meteor.userId()}, {assigned: Meteor.userId()}]}).fetch();
    }

    getAssistants() {
        return Profiles.find({assisting: Meteor.userId()}, {sort: {firstName: +1}}).fetch();
    }

    handleChange(e) {
        e.preventDefault();
        let customerID = e.target.value;
        let fromCustomer = Customers.findOne({_id: customerID});
        let customerName = fromCustomer.firstName;
        Session.set("custID", customerID);
        Session.set("CN", customerName);
    }

    assignTo(e) {
        e.preventDefault();
        let assistantID = e.target.value;
        let assignedTo = Profiles.findOne({_id: assistantID});
        let assistant = assignedTo.user;
        Session.set("assistantName", assistant);
    }

    addProject() {
        if (this.state.submitType === "save") {
            var custId = Session.get("custID");
            var custName = Session.get("CN");

            var assistant = Session.get("assistantName");

            if (!assistant) {
                var assistantID = Profiles.findOne({user: Meteor.userId()});
                assistant = assistantID.user;
            }

            //var startDate = this.state.dateStart;
            //var endDate = this.state.dateEnd;

            var startDate = new Date();
            var endDate = new Date(moment().add(1, 'days'));

            var project = this.refs.proj_name.value.trim();
            //var tomorrow = new Date(moment().add(1, 'days'));
            let addNewProj = "Added new project: " + project;

            if (endDate > startDate) {
                if (project) {
                    Meteor.call('addCustomerAssistant', custId, assistant);
                    Meteor.call('addProject', custId, custName, project, startDate, endDate, assistant, function(err, res) {
                        let projID = res;
                        Meteor.call('addProjDetail', project, projID, "NA", addNewProj, custId, "fa fa-plus-square fa-lg", assistant);
                    });
                }
            }
        }
    }

    cancelButton() {
        this.setState({submitType: "cancel"});
    }

    saveButton() {
        this.setState({submitType: "save"});
    }

    render() {
        let customerList = this.getCustomers().map( (customerNames) => {
            return <option ref="customerID" key={customerNames._id} customerNames={customerNames} value={customerNames._id} >{customerNames.firstName}</option>
        })

        let assistantList = this.getAssistants().map( (assistantNames) => {
            return <option ref="assistantID" key={assistantNames._id} assistantNames={assistantNames} value={assistantNames._id}> {assistantNames.firstName} {assistantNames.lastName}</option>
        })

        return (
            <div className="editProj">
                <form onSubmit={this.addProject.bind(this)}>
                    <div className="col-xs-12" style={{height:'120px'}}>
                        <div className="h3-modal text-center">Add New Project</div>
                        <div className="row">
                            <div className="col-xs-5">
                                <select label="Select Customer" className="col-xs-12" style={{height:'34px'}} onChange={this.handleChange.bind(this)}>
                                    <option>Select Customer</option>
                                    {customerList}
                                </select>

                            </div>
                            <div className="col-xs-7 zero-padding">
                                <input ref="proj_name" type="text" placeholder="Project Name" className="form-control"/>
                            </div>
                        </div>
                        {/*<div className="row">
                            <div className="col-xs-12">
                                <div className="col-xs-2"/>
                                <div className="col-xs-4 zero-padding">
                                    <DateTimeField onChange={this.handleChangeStart.bind(this)} />
                                </div>
                                <div className="col-xs-4 zero-padding">
                                    <DateTimeField onChange={this.handleChangeEnd.bind(this)} />
                                </div>
                            </div>
                        </div>*/}
                        <div className="row">
                            <div className="col-xs-12">
                                <label className="col-xs-2">Assign to:</label>
                                <select label="Me" className="col-xs-5" onChange={this.assignTo.bind(this)}>
                                    <option>Me</option>
                                    {assistantList}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="row col-xs-12">

                    </div>
                    <div className="col-xs-12 modal-save" style={{height:'10px'}}/>
                    <div className="row"/>
                    <div className="col-xs-12">
                        <button className="btn btn-warning col-xs-3 text-center" type="submit" onClick={this.saveButton.bind(this)}>CREATE</button>
                        <button className="btn btn-default col-xs-3 pull-right text-center" type="submit" onClick={this.cancelButton.bind(this)}>CANCEL</button>
                    </div>
                </form>
            </div>
        )
    }
}
