import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import moment from "moment";
import {Timeline, TimelineEvent} from 'react-event-timeline';

ProjectDetails = new Mongo.Collection("details");

export default class ProjHistory extends TrackerReact(React.Component) {
    constructor(props) {
        super(props);
        this.state = {
            subscription: {
                allDetails: Meteor.subscribe("projDetails"),
                customers: Meteor.subscribe("listCustomer"),
                profiles: Meteor.subscribe("listProfiles"),
            }
        };
    }

    getDetailsDB() {
        let projID = Session.get('projectID');
        var detailsList = ProjectDetails.find({projectID: projID}).fetch();
        return detailsList;
    }

    getProjectList() {
        var projects = ProjectDetails.find().fetch();
        var distinctProjects = _.uniq(projects, false, function( project ) {return project.projectID});
        var projectList = _.pluck(distinctProjects, 'projectID');
        return projectList;
    }

    getProfile(userID) {
        var profileName;
        var profileDB = Profiles.findOne({user: userID});

        if (profileDB.firstName) {
            var profileFirstName = profileDB.firstName;
            var profileLastName = profileDB.lastName;
            profileName = profileFirstName + " " + profileLastName;
        }
        return profileName;
    }

    getProjDetails(e) {
        e.preventDefault();
        let projID = e.target.value;
        Session.set('projectID', projID);
    }

    getAssistants() {
        return Profiles.find({assisting: Meteor.userId()}).fetch();
    }

    render() {
        let projectsID = this.getProjectList().map(currProjID => {
            let proj = ProjectDetails.findOne({projectID: currProjID});
            return <option ref="projID" value={currProjID} key={currProjID}>{proj.projectName}</option>
        })

        let assistantList = this.getAssistants().map( (assistantNames) => {
            return <option ref="assistantID" key={assistantNames._id} assistantNames={assistantNames} value={assistantNames._id}> {assistantNames.firstName} {assistantNames.lastName}</option>
        })

        return (
            <div>
                <div className="col-xs-12">
                    <h2 id="projectsTitle" className="officialColor">Project History</h2>
                </div>
                <div className="col-xs-12">
                    <select className="col-xs-4" onChange={this.getProjDetails.bind(this)}>
                        <option>Select Project</option>
                        {projectsID}
                    </select>
                </div>
                <div className="col-xs-12">
                  <ul style={{padding: '0px'}}>
                      {this.getDetailsDB().map( details =>{
                          return (
                              <div key={details._id} details={details}>
                                  <div className="col-xs-12">
                                    <Timeline>
                                      <TimelineEvent title={this.getProfile (details.user)}
                                                     createdAt={moment(details.createdAt).format('MMM DD YY, h:mm A')}
                                                     icon={<i className={details.iconfig}></i>}
                                      >
                                          {details.detail}
                                      </TimelineEvent>
                                    </Timeline>
                                  </div>
                              </div>
                          )
                      })}
                  </ul>
                </div>
            </div>
        )
    }
}
