import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import DateTimeField from 'react-bootstrap-datetimepicker';
import moment from 'moment';

export default class AddChildTask extends TrackerReact(React.Component) {

    constructor(){
        super();

        this.state = {
            submitType: "cancel",
            dateStart: moment().toDate(),
            dateEnd: moment().toDate()
        }
    }

    handleChange(e) {
        e.preventDefault();
        this.state.value = e.target.value;
    }

    addTask(e) {
        if (this.state.submitType === "save") {
            let project = Session.get('projectID');
            var task = this.refs.taskdesc.value.trim();
            var startDate = this.state.dateStart;
            var endDate = this.state.dateEnd;
            var taskCount = TasksCollections.find({projectID: project}).count();
            var childLevel = Session.get('childLevel') + 1;
            var pID = Session.get('parentID');
            let prevSubID = Session.get('subID');
            var newID = pID;
            let newTask = "Added new task: " + task;
            let assistant = Session.get('assistantName');
            var newSubID;

            var projectFetch = ProjectsCollections.find({_id:project}).fetch();
            var projStatus = projectFetch[0].disp;
            var projectName = projectFetch[0].projectName;
            var mainAdmin = projectFetch[0].user;

            var timeStart = TasksCollections.find({subID: prevSubID, startDate: {$lte: startDate}}).count();
            var timeEnd = TasksCollections.find({subID: prevSubID, endDate: {$gte: endDate}}).count();

            var parentTask = TasksCollections.findOne({subID: prevSubID});

            let nowDateTime = new Date();

            if (!assistant) {
                var assistantID = Profiles.findOne({user: Meteor.userId()});
                assistant = assistantID.user;
            }

            if (endDate < startDate){
                e.preventDefault();
                Bert.alert('End time is before Start time', 'danger', 'fixed-top');
            }
            else if (projStatus === "locked") {
                    if (timeStart === 0) {
                        e.preventDefault();
                        Bert.alert( 'Start time is before parent child task start time', 'danger', 'fixed-top');
                    } else if (timeEnd === 0) {
                        e.preventDefault();
                        Bert.alert( 'End time is after parent child task end time', 'danger', 'fixed-top');
                    } else if (nowDateTime < endDate) {
                        e.preventDefault();
                        Bert.alert('Task is already overdue', 'danger', 'fixed-top');
                    } else if (startDate < moment(parentTask.endDate, "x").toDate()) {
                        e.preventDefault();
                        Bert.alert('Child task starts before parent task ends', 'danger', fixed-top);
                    }
                    else {
                        if (task) {
                            Meteor.call('addTask', project, pID, prevSubID, task, startDate, endDate, childLevel, assistant, mainAdmin, function(err, res) {
                                taskID = res;
                                Meteor.call('updateTaskSub', taskID, newSubID);
                                Meteor.call('addProjDetail', projectName, project, taskID, newTask, "NA", "fa fa-plus-square fa-lg", assistant);
                            });

                            {/*while (childLevel > 0) {
                                var parentStart = TasksCollections.find({_id: newID, startDate: {$lte: startDate}}).count();
                                if (parentStart === 0) {
                                    Meteor.call('updateTaskStart', newID, startDate);
                                }

                                var parentEnd = TasksCollections.find({_id: newID, endDate: {$gte: endDate}}).count();
                                if (parentEnd === 0) {
                                    Meteor.call('updateTaskEnd', newID, endDate);
                                }
                                var findNextParent = TasksCollections.findOne({_id: newID});
                                newID = findNextParent.parentID;
                                childLevel = childLevel - 1;
                            }*/}
                        }
                        else {
                            e.preventDefault();
                            Bert.alert('Task name is invalid/empty', 'danger', 'fixed-top');
                        }
                    }
                }
            else if (projStatus === "unlocked"){
                if (task) {
                    if (nowDateTime > timeEnd) {
                        Meteor.call('addTask', project, pID, prevSubID, task, startDate, endDate, childLevel, assistant, mainAdmin, function(err, res) {
                            taskID = res;
                            newSubID = prevSubID + taskID;                            
                            Meteor.call('updateTaskSub', taskID, newSubID);
                            Meteor.call('addProjDetail', projectName, project, taskID, newTask, "NA", "fa fa-plus-square fa-lg", assistant);
                        });

                        {/*while (childLevel > 0) {
                            var parentStart = TasksCollections.find({subID: prevSubID, startDate: {$lte: startDate}}).count();
                            if (parentStart === 0) {
                                Meteor.call('updateTaskStart', newID, startDate);
                            }

                            //var parentEnd = TasksCollections.find({_id: newID, endDate: {$gte: endDate}}).count();
                            //if (parentEnd === 0) {
                            //    Meteor.call('updateTaskEnd', newID, endDate);
                            //}
                            var findNextParent = TasksCollections.findOne({_id: newID});
                            newID = findNextParent.parentID;

                            childLevel = childLevel - 1;
                        }*/}

                        if (timeStart === 0 | taskCount === 0) {
                            Meteor.call('updateProjectStart', project, startDate);
                        }

                        if (timeEnd === 0 | taskCount === 0) {
                            Meteor.call('updateProjectEnd', project, endDate);
                        }
                    }
                }
            }
        }
    }

    cancelButton() {
        this.setState({submitType: "cancel"});
    }

    saveButton() {
        this.setState({submitType: "save"});
    }

    handleChangeStart(newStart) {
        var newStartTemp = moment(newStart, "x").toDate();
        this.setState({dateStart: newStartTemp});
    }

    handleChangeEnd(newEnd) {
        var newEndTemp = moment(newEnd, "x").toDate();
        this.setState({dateEnd: newEndTemp});
    }

    getAssistants() {
        return Profiles.find({assisting: Meteor.userId()}, {sort: {firstName: +1}}).fetch();
    }

    assignTo(e) {
        e.preventDefault();
        let assistantID = e.target.value;
        let assignedTo = Profiles.findOne({_id: assistantID});
        let assistant = assignedTo.user;
        Session.set("assistantName", assistant);
    }

    render() {
        let assistantList = this.getAssistants().map( (assistantNames) => {
            return <option ref="assistantID" key={assistantNames._id} assistantNames={assistantNames} value={assistantNames._id}> {assistantNames.firstName} {assistantNames.lastName}</option>
        })
        return (
            <div className="addTask">
                <form onSubmit={this.addTask.bind(this)}>
                    <div className="col-xs-12" style={{height:'85px'}}>
                        <div className="h3-modal text-center">Add New Task</div>
                        <div className="row">
                            <div className="col-xs-6" >
                                <input className="col-xs-12" ref="taskdesc" placeholder="Task Description"></input>
                            </div>
                            <div className="col-xs-3 zero-padding">
                                <DateTimeField onChange={this.handleChangeStart.bind(this)} />
                            </div>
                            <div className="col-xs-3 zero-padding">
                                <DateTimeField onChange={this.handleChangeEnd.bind(this)} />
                            </div>
                        </div>

                    </div>
                    <div className="col-xs-12" style={{height:'40px'}}>
                        <label className="col-xs-2">Assign to:</label>
                        <select label="Me" className="col-xs-5" onChange={this.assignTo.bind(this)}>
                            <option>Me</option>
                            {assistantList}
                        </select>
                    </div>
                    <div className="col-xs-12 modal-save" style={{height:'10px'}}/>
                    <div className="col-xs-12">
                        <button className="btn btn-warning col-xs-3 text-center" type="submit" onClick={this.saveButton.bind(this)}>SAVE</button>
                        <button className="btn btn-default col-xs-3 pull-right text-center" type="submit" onClick={this.cancelButton.bind(this)}>CANCEL</button>
                    </div>
                </form>
            </div>
        )
    }
}
