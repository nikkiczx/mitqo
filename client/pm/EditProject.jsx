import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class EditProject extends TrackerReact(React.Component) {

    constructor(){
        super();
        this.state = {
            customerID: "",
            submitType: "cancel",
            subscription: {
                customers: Meteor.subscribe("listCustomer"),
            }
        }
    }

    componentWillUnmount() {
        this.state.subscription.customers.stop();
    }

    getCustomers() {
        return Customers.find().fetch();
    }

    handleChange(e) {
        e.preventDefault();
        let custID = e.target.value;
        this.setState({customerID: custID});
    }

    projNameChange(e) {
        e.preventDefault();
        this.setState({projectName: e.target.value});
    }

    editProject() {
        if (this.state.submitType === "save") {            
            var customer = this.state.customerID;
            var project = this.state.projectName;
            var projectID = Session.get('projectID');
            var assistant = Session.get("assistantName");
            var changes;

            if (project) {
                changes = "Renamed project from '" + Session.get('projectName') + "' to '" + project + "'";
                Meteor.call('addProjDetail', project, projectID, "NA", changes, customer, "fa fa-pencil-square-o fa-lg", assistant);
            } else {
                project = Session.get('projectName');
            }

            if (!assistant) {
                var assistantID = Profiles.findOne({user: Meteor.userId()});
                assistant = assistantID.user;
                changes = "Reassigned project '" + Session.get('projectName') + "' to " + assistant;
                Meteor.call('addCustomerAssistant', customer, assistant);
                Meteor.call('addProjDetail', project, projectID, "NA", changes, customer, "fa fa-pencil-square-o fa-lg", assistant);
            }

            Meteor.call('editProject', projectID, project, customer, assistant);
        }
    }

    cancelButton() {
        this.setState({submitType: "cancel"});
    }

    saveButton() {
        this.setState({submitType: "save"});
    }

    getAssistants() {
        return Profiles.find({assisting: Meteor.userId()}, {sort: {firstName: +1}}).fetch();
    }

    assignTo(e) {
        e.preventDefault();
        let assistantID = e.target.value;
        let assignedTo = Profiles.findOne({_id: assistantID});
        let assistant = assignedTo.user;
        Session.set("assistantName", assistant);
    }

    render() {
        let customerList = this.getCustomers().map( (customerID) => {
            return <option ref="customerID" key={customerID._id} value={customerID._id}>{customerID.firstName}</option>
        })

        let assistantList = this.getAssistants().map( (assistantNames) => {
            return <option ref="assistantID" key={assistantNames._id} assistantNames={assistantNames} value={assistantNames._id}> {assistantNames.firstName} {assistantNames.lastName}</option>
        })

        this.state.customerID = Session.get('customerID');

        let projName = Session.get('projectName');

        return (
            <div className="editProj">
                <form onSubmit={this.editProject.bind(this)}>
                    <div className="col-xs-12" style={{height:'125px'}}>
                        <div className="h3-modal text-center">Edit Project</div>
                        <div className="row">
                            <div className="col-xs-5">
                                <select className="col-xs-12" value={this.state.customerID} style={{height:'34px'}} onChange={this.handleChange.bind(this)}>
                                    {customerList}
                                </select>
                            </div>
                            <div className="col-xs-7 zero-padding">
                                <input ref="proj_name" placeholder={projName} type="text" onChange={this.projNameChange.bind(this)} className="form-control"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12">
                                <label className="col-xs-2">Assign to:</label>
                                <select label="Me" className="col-xs-5" onChange={this.assignTo.bind(this)}>
                                    <option>Me</option>
                                    {assistantList}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 modal-save" style={{height:'10px'}}/>
                    <div className="row"/>
                    <div className="col-xs-12">
                        <button className="btn btn-warning col-xs-3 text-center" type="submit" onClick={this.saveButton.bind(this)}>SAVE</button>
                        <button className="btn btn-default col-xs-3 pull-right text-center" type="submit" onClick={this.cancelButton.bind(this)}>CANCEL</button>
                    </div>
                </form>
            </div>
        )
    }
}
