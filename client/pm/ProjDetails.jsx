import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import moment from "moment";

export default class ProjDetails extends TrackerReact(React.Component) {
    constructor(props) {
        super(props);
        this.state = {
            subscription: {
                allDetails: Meteor.subscribe("projDetails"),
                customers: Meteor.subscribe("listCustomer"),
                profiles: Meteor.subscribe("listProfiles"),
            }
        };
    }

    getProjDoc(e) {
        e.preventDefault();
        let projName = e.target.value;
        Session.set('projectName', projName);
    }

    getProjectList() {
        var projects = ProjectsCollections.find({$or: [{user: Meteor.userId()}, {assigned: Meteor.userId()}]}).fetch();
        return projects;
    }

    getTaskList() {
        let projName = Session.get('projectName');
        let projID; let taskList = [];
        var projList = ProjectsCollections.findOne({projectName: projName});
        if (projList) {
            projID = projList._id;
            Session.set('projectID', projID);
            taskList = TasksCollections.find({projectID: projID}, {sort: {createdAt: +1}}).fetch();
        }
        return taskList;
    }

    /*getDetailsDB() {
        let projID = Session.get('projectID');
        var detailsList = ProjectDetails.find({projectName: projID}, {sort: {createdAt: +1, taskID: +1}}).fetch();
        console.log(detailsList);
        return detailsList;
    }*/

    getDetailsDB(taskID) {
        let detailsList = [];
        detailsList = ProjectDetails.find({taskID: taskID}).fetch();
        return detailsList;
    }

    getProjDetails() {
        let projID = Session.get('projectID');
        let detailsList = [];
        detailsList = ProjectDetails.find({$and: [{taskID: "NA"},{projectID: {$eq: projID}}]}).fetch();
        return detailsList;
    }

    findTaskName(taskID) {
        var itemName;
        if (taskID === "NA") {
            itemName = Session.get('projectName');
        } else {
            let taskDoc = TasksCollections.findOne({_id: taskID});
            if (taskDoc) {
                itemName = taskDoc.taskName;
            }
        }

        return itemName;
    }

    render() {
        let projectsID = this.getProjectList().map( projs => {
            return <option ref="projID" key={projs._id} projs={projs}>{projs.projectName}</option>
        })
        return (
            <div>
                <div className="col-xs-12">
                    <h2 id="projectsTitle" className="officialColor">Project Details</h2>
                </div>
                <div className="col-xs-12 table-fonts">
                    <select className="col-xs-4" onChange={this.getProjDoc.bind(this)}>
                        <option>Select Project</option>
                        {projectsID}
                    </select>
                </div>
                <div className="col-xs-12">
                  <ul style={{padding: '0px'}}>
                      <h3 className="col-xs-12 table-fonts">
                          <div className="col-xs-3">Tasks</div>
                          <div className="col-xs-9">
                              <div className="col-xs-3">Date</div>
                              <div className="col-xs-9">Details</div>
                          </div>
                      </h3>
                      <div className="col-xs-12 table-fonts">
                          {this.getProjDetails().map( proj =>{
                              return (
                                  <div key={proj._id} proj={proj}>
                                      <div className="col-xs-3">{Session.get('projectName')}</div>
                                      <div className="col-xs-9">
                                          <div className="col-xs-3">{moment(proj.createdAt).format('MMM DD YY, h:mm A')}</div>
                                          <div className="col-xs-9">{proj.detail}</div>
                                      </div>
                                  </div>
                              )
                          })}
                      </div>
                      <div>
                          {this.getTaskList().map( tasks =>{
                              return (
                                  <div key={tasks._id} tasks={tasks}>
                                      <div className="col-xs-12 table-fonts">
                                          <div className="col-xs-3">{tasks.taskName}</div>
                                          <div className="col-xs-9">
                                              {this.getDetailsDB(tasks._id).map( details =>{
                                                  return (
                                                      <div key={details._id} details={details}>
                                                          <div className="col-xs-3">{moment(details.createdAt).format('MMM DD YY, h:mm A')}</div>
                                                          <div className="col-xs-9">{details.detail}</div>
                                                      </div>
                                                  )
                                              })}
                                          </div>
                                      </div>
                                  </div>
                              )
                          })}
                      </div>
                      {/*{this.getDetailsDB().map( details =>{
                          return (
                              <div key={details._id} details={details}>
                                  <div className="col-xs-12 table-fonts">
                                      <div className="col-xs-2">{this.findTaskName(details.taskID)}</div>
                                      <div className="col-xs-2">{moment(details.createdAt).format('MMM DD YY, h:mm A')}</div>
                                      <div className="col-xs-8">{details.detail}</div>
                                  </div>
                              </div>
                          )
                      })}*/}
                  </ul>
                </div>
            </div>
        )
    }
}
