import React from 'react';
import Rodal from 'rodal';
import DateTimeField from 'react-bootstrap-datetimepicker';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import moment from "moment";
import Timeline from 'react-calendar-timeline';

import AddProject from './AddProject.jsx';
import ProjectsTable from './ProjectsTable.jsx';
import TasksTable from './TasksTable.jsx';

ProjectsCollections = new Mongo.Collection("projects");
TasksCollections = new Mongo.Collection("tasks");

export default class ProjectsPage extends TrackerReact(React.Component) {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            subscription: {
                allDetails: Meteor.subscribe("projDetails"),
                customers: Meteor.subscribe("listCustomers"),
                VSprojects: Meteor.subscribe("listProjects"),
                allTasks: Meteor.subscribe("taskList")
            }
        };
    }

    /*componentWillUnmount() {
        this.state.subscription.projDetails.stop();
        this.state.subscription.customers.stop();
        this.state.subscription.VSprojects.stop();
        this.state.subscription.allTasks.stop();
    }*/

    getCustomers() {
        return Customers.find({assigned: Meteor.userId()}).fetch();
    }

    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }

    getTableGroups(){
        var index = 0; var temp = [];

        let tempArray = this.getProjectsDB().map( projects => {
            index = index + 1;
            temp.push({id: index, group: "group " + index});
            this.getTasks(projects._id).map( tasks => {
                index = index + 1;
                temp.push({id: index, group: "group " + index});
                return index;
            });

            return index;
        });

        return temp;
    }

    getTableItems(){
        var index = 0; var temp = []; var projName; var startTime; var endTime; var taskStatus;
        var dateTodayNow = moment(); var taskClassName; var tomorrow = new Date(moment().add(1, 'days'));

        let tempArray = this.getProjectsDB().map( projects =>{
            index = index + 1;
            projName = projects.projectName;
            startTime = projects.startDate;
            endTime = projects.endDate;
            if (endTime > dateTodayNow) {

            }
            temp.push({id: index, group: index, title: projName, start_time: startTime, end_time: endTime, canMove: false, canResize: false});
            this.getTasks(projects._id).map( tasks => {
                taskClassName = 'tasks';
                index = index + 1;
                projName = tasks.taskName;
                startTime = tasks.startDate;
                endTime = tasks.endDate;
                taskStatus = tasks.progress;

                if (endTime < dateTodayNow) {
                    if (taskStatus === "completed") {
                        taskClassName = 'completed';
                    } else {
                        taskClassName = 'overdue';
                    }
                } else if (endTime < tomorrow) {
                    if (taskStatus === 'completed') {
                      taskClassName = 'completed';
                    } else {
                      taskClassName = 'dueSoon';
                    }
                }
                temp.push({id: index, group: index, title: projName, start_time: startTime, end_time: endTime, canMove: false, canResize: false, className: taskClassName});
            });

            return index;
        });

        return temp;
    }

    getProjectsDB() {
        var projects = ProjectsCollections.find({$and: [{progress: {$ne: "deleted"}}, {$or: [{user: Meteor.userId()}, {assigned: Meteor.userId()}]}]}).fetch();
        return projects;
    }

    getTasks(projID) {
        var Tasks;
        var projDetails = ProjectsCollections.find({_id: projID}).fetch();
        var projAssigned = projDetails[0].assigned;
        var projUser = projDetails[0].user;
        if (projAssigned === Meteor.userId() | projUser === Meteor.userId()) {
            Tasks = TasksCollections.find({projectID: projID}, {sort: {parentID: +1, subID: +1, startDate: +1}}).fetch();
        } else {
            Tasks = TasksCollections.find({$and: [{projectID: projID}, {$or: [{user: Meteor.userId()}, {assigned: Meteor.userId()}]}]}, {sort: {parentID: +1, subID: +1, startDate: +1}}).fetch();
        }

        return Tasks;
    }

    render() {
        let customerList = this.getCustomers().map( (customerNames) => {
            return <option ref="customerID" key={customerNames._id} value={customerNames._id}>{customerNames.firstName}</option>
        })

        return (
            <div>
                <div className="row col-xs-12">
                    <div className="col-xs-6 text-right">
                        <h2 id="projectsTitle"><a href="/details" className="officialColor">PROJECTS</a></h2>
                        </div>
                        <div className="col-xs-6" style={{left: '-20px', top:'25px'}}>
                            <i onClick={this.show.bind(this)}>
                                <img src="/images/plus_icon.png" width="60px"></img>
                                    <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                                        <AddProject />
                                    </Rodal>
                            </i>
                  </div>
              </div>
              <div className="row col-xs-12 zero-margin table-fonts" style={{top:'20px'}}>
                  <div className="col-xs-8" style={{padding:'0px'}}>
                      <div className="col-xs-1" style={{width:'70px'}}>
                          <label>Customer</label>
                      </div>
                      <div className="col-xs-7">
                          <label>Project </label>
                      </div>
                      <div className="col-xs-3 text-center">
                          <label>Assigned To</label>
                      </div>
                 </div>
                 <div className="col-xs-6">

                 </div>
              </div>
              <div className="col-lg-7 col-xs-9 zero-padding table-fonts project-font" style={{top:'46px'}}>
                  <ul style={{padding: '0px'}}>
                      {this.getProjectsDB().map( project =>{
                          return (
                              <ProjectsTable key={project._id} project={project} />
                          )
                      } )}
                  </ul>
              </div>
              <div>
                  <div className="col-lg-5 col-xs-3" style={{top: '-10px'}}>
                      <Timeline groups={this.getTableGroups()}
                          items={this.getTableItems()}
                          defaultTimeStart={moment().add(-12, 'hour')}
                          defaultTimeEnd={moment().add(12, 'hour')}
                      />
                  </div>
              </div>
          </div>
            )
        }
    }
