import React from 'react';
import moment from "moment";
import Rodal from 'rodal';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactDOM from 'react-dom';
import ReactTooltip from 'react-tooltip'

import AddTask from './AddTask.jsx';
import TasksTable from './TasksTable.jsx';
import EditProject from './EditProject.jsx';
import DeleteModal from './DeleteModal.jsx';

export default class ProjectsTable extends TrackerReact(React.Component) {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            visible2: false,
            visible3: false,
            visible4: false,
            subscription: {
                customers: Meteor.subscribe("listCustomers"),
                allTasks: Meteor.subscribe("taskList"),
                profiles: Meteor.subscribe("listProfiles")
            }
        };
    }

    show() {
        this.setState({ visible: true });

        let projectID = this.props.project._id;
        Session.set('projectID', projectID);
    }

    hide() {
        this.setState({ visible: false });
    }

    getTasks() {

      var Tasks;
      var projDetails = ProjectsCollections.find({_id: this.props.project._id}).fetch();
      var projAssigned = projDetails[0].assigned;
      var projUser = projDetails[0].user;
      if (projAssigned === Meteor.userId() | projUser === Meteor.userId()) {
          Tasks = TasksCollections.find({projectID: this.props.project._id}, {sort: {parentID: +1, subID: +1, startDate: +1}}).fetch();
      } else {
          Tasks = TasksCollections.find({$and: [{projectID: this.props.project._id}, {$or: [{user: Meteor.userId()}, {assigned: Meteor.userId()}]}]}, {sort: {parentID: +1, subID: +1, startDate: +1}}).fetch();
      }

      return Tasks;
    }

    showDelete() {

        let projectName = this.props.project.projectName;
        Session.set('itemName', projectName);

        let projectID = this.props.project._id;
        Session.set('projectID', projectID);

        let customerID = this.props.project.customerID;
        Session.set('customerID', customerID);

        if (this.props.project.user === Meteor.userId()) {
            this.setState({ visible3: true });
        } else {
            Bert.alert('Warning! Unauthorized to delete project.', 'danger', 'fixed-top');
        }
    }

    hideDelete() {
        this.setState({ visible3: false});
    }

    lockUnlockProject() {
        if (this.props.project.disp === "locked") {
            Meteor.call('toggleLockUnlockProject', this.props.project._id, "unlocked");
            Meteor.call('addProjDetail', this.props.project._id, "NA", "Unlocked Project", this.props.project.customerID, "fa fa-unlock fa-lg");
        } else {
            Meteor.call('toggleLockUnlockProject', this.props.project._id, "locked");
            Meteor.call('addProjDetail', this.props.project._id, "NA", "Locked Project", this.props.project.customerID, "fa fa-lock fa-lg");
        }
    }

    archiveProj() {
        this.setState({visible4: true});
    }

    hideArchive() {
        this.setState({visible4: false});
    }

    yesArchive() {
        this.setState({submitType: "save"});
    }

    cancelArchive() {
        this.setState({submitType: "cancel"});
    }

    archiveProjSave() {
        if (this.state.submitType === "save") {
            let nowDate =  new Date();
            let projectID = this.props.project._id;
            let projUpdate = "Entire project archived on " + nowDate;

            let projDetails = ProjectsCollections.findOne({_id: projectID});

            Meteor.call('archiveProject', projectID, projDetails.customerID, projDetails.customerName, projDetails.projectName, projDetails.startDate, projDetails.endDate, projDetails.progress, projDetails.assigned);
            Meteor.call('deleteProject', projectID);
            Meteor.call('addProjDetail', projDetails.projectName, projectID, "NA", projUpdate, projDetails.customerID, "fa fa-archive fa-lg", Meteor.userId());

            let tasks = TasksCollections.find({projectID: projectID}).fetch().map( task => {
                Meteor.call('archiveTask', projectID, task.parentID, task.subID, task.taskID, task.taskName, task.startDate, task.endDate, task.child, task.assigned, task.progress);
                Meteor.call('deleteTask', task._id);
            });

            Bert.alert({
              hideDelay: 100,
              message: 'Project Archive Successful',
              type: 'success',
              style: 'fixed-top',
              icon: 'fa-check'
            });
        }
    }

    editRow() {

        if (this.props.project.disp == "locked") {
            if (this.props.project.user === Meteor.userId() | this.props.project.assigned === Meteor.userId()) {
                let projectID = this.props.project._id;
                Session.set('projectID', projectID);

                let projectName = this.props.project.projectName;
                Session.set('projectName', projectName);

                let customerID = this.props.project.customerID;
                Session.set('customerID', customerID);

                this.setState({ visible2: true});
            } else {
                Bert.alert('Warning! Unauthorized to edit project.', 'danger', 'fixed-top');
            }
        } else {
            let projectID = this.props.project._id;
            Session.set('projectID', projectID);

            let projectName = this.props.project.projectName;
            Session.set('projectName', projectName);

            let customerID = this.props.project.customerID;
            Session.set('customerID', customerID);

            this.setState({ visible2: true});
        }
    }

    render() {
        var startDate = moment(this.props.project.startDate).format('MMM DD YY, h:mm A');
        var endDate = moment(this.props.project.endDate).format('MMM DD YY, h:mm A');

        var customerID = this.props.project.customerID;
        var customerProfile = Customers.find({_id: customerID}).fetch();
        var customerFirstName = customerProfile[0].firstName;
        var customerLastName = customerProfile[0].lastName;
        var customerInitials = customerFirstName.charAt(0) + customerLastName.charAt(0);
        var customerName = customerFirstName + " " + customerLastName;

        var assistantID = this.props.project.assigned;
        var assistantProfile = Profiles.findOne({user: assistantID});
        var assistantInitials = ""; var assistantName = "";

        if (assistantProfile) {
              if (assistantID !== Meteor.userId()) {
              assistantInitials = assistantProfile.firstName.charAt(0) + assistantProfile.lastName.charAt(0);
              assistantName = assistantProfile.firstName + " " + assistantProfile.lastName;
              }
        }

        var projectID = this.props.project._id;

        let nowDateTime = new Date();
        var projectOverdueHighlight;
        var dispIcon;

        var projEndDate = this.props.project.endDate; var projDisp = this.props.project.disp;

        if (projEndDate < nowDateTime) {
            projectOverdueHighlight = "#ff8080";
        }
        if (projDisp === "locked") {
            dispIcon = "/images/locked.png";
        }
        else {
            dispIcon = "/images/unlocked.png"
        }

        return (
            <div>
                <div>
                    <div className="col-xs-1 col-lg-1 zero-padding" style={{width:'12px', bottom: '1px'}} >
                        <i onClick={this.lockUnlockProject.bind(this)}><img src={dispIcon} width="15px"></img></i>
                    </div>
                    <text className="col-xs-2 col-lg-2 text-left" style={{width:'50px'}} data-tip={customerName}>{customerInitials}</text>
                    <div className="col-xs-1 col-lg-1 zero-padding" style={{width:'12px', bottom: '2px'}} >
                        <i onClick={this.show.bind(this)}><img src="/images/square-plus-icon.png" width="12px"></img>
                            <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                                <AddTask />
                            </Rodal>
                        </i>
                    </div>
                    <div className="row table-row-hover" style={{background: projectOverdueHighlight}} onClick={this.editRow.bind(this)}>
                      <div className="row">
                        <Rodal visible={this.state.visible2} onClose={this.hide.bind(this)}>
                            <EditProject />
                        </Rodal>
                        <text className="col-xs-6 col-lg-6 text-left row-project-padding" style={{width:'480px'}}>{this.props.project.projectName}</text>
                        <i onClick={this.showDelete.bind(this)} className="col-xs-1 zero-padding" style={{width:'15px'}}><img src="/images/delete-icon.png" width="15px"></img>
                            <Rodal visible={this.state.visible3} onClose={this.hideDelete.bind(this)}>
                                <DeleteModal />
                            </Rodal>
                        </i>
                        <ReactTooltip />
                        <text className="col-xs-2 col-lg-2 row-project-padding" data-tip={assistantName} style={{width:'25px'}}>{assistantInitials}</text>
                        <i onClick={this.archiveProj.bind(this)} className="col-xs-1 zero-padding fa fa-archive" style={{width: '15px', left: '8px'}}>
                            <Rodal visible={this.state.visible4} onClose={this.hideArchive.bind(this)}>
                                <div className="col-xs-12 editProj table-fonts">
                                  <form onSubmit={this.archiveProjSave.bind(this)}>
                                      <div className="row col-xs-12 h3-modal text-center">Archive</div>
                                      <div className="row col-xs-12 h3-modal pull-left">Project {this.props.project.projectName}</div>
                                      <div className="row col-xs-12 zero-padding text-center" style={{top:'10px'}}>Are you sure you want to archive this project?</div>
                                      <div className="col-xs-12 modal-save" style={{height:'10px', top:'15px'}}/>
                                      <div className="col-xs-12" style={{top:'25px'}}>
                                          <button className="btn btn-warning col-xs-3 text-center" type="submit" onClick={this.yesArchive.bind(this)}>YES</button>
                                          <button className="btn btn-default col-xs-3 pull-right text-center" type="submit" onClick={this.cancelArchive.bind(this)}>CANCEL</button>
                                      </div>
                                  </form>
                                </div>
                            </Rodal>
                        </i>
                      </div>
                      {/*<div className="col-lg-12 subline-fonts" style={{left: '80px', height:'10px'}}></div>*/}
                    </div>
                </div>
                <div className="task-font zero-padding">
                    <ul>
                        {this.getTasks().map ( task =>{
                            return <TasksTable key={task._id} task={task} />
                        } )}
                    </ul>
                </div>
            </div>
        )
    }
}
