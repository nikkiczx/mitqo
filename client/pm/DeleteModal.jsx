import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import moment from 'moment';

export default class DeleteModal extends TrackerReact(React.Component) {

    constructor(){
        super();

        this.state = {
            submitType: "cancel",
            subID: "NA",
            subscription: {
                customers: Meteor.subscribe("listCustomer"),
                projects: Meteor.subscribe("listProjects"),
                tasks: Meteor.subscribe("taskList"),
            }
        }
    }

    cancelButton() {
        this.setState({submitType: "cancel"});
    }

    getCustomer() {
        let customerID = Session.get('customerID');
        let customer = Customers.find({_id: customerID}).fetch();

        return customer[0];
    }

    deleteItem() {
        let subID = Session.get('subID');
        let projectID = Session.get('projectID'); let details;

        if (this.state.submitType === "save") {
            if (subID) {
                let getTasks = TasksCollections.find({$and: [{projectID: projectID}, {subID: {$regex: subID}}]}).fetch();
                let getProject = ProjectsCollections.find({_id: projectID}).fetch();

                getTasks.map( task => {
                    details = "Deleted task '" + task.taskName + "' for project '" + getProject[0].projectName + "'";
                    Meteor.call('addProjDetail', getProject[0].projectName, projectID, task._id, details, getProject[0].customerID, "fa fa-trash fa-lg");
                    Meteor.call('deleteTask', task._id);
                });
            } else if (projectID) {
                let tasksList = TasksCollections.find({projectID: projectID}).fetch();
                let projDetails = ProjectsCollections.find({_id: projectID}).fetch();
                details = "Deleted entire project of '" + Session.get('itemName') + "'";

                Meteor.call('deleteProject', projectID);
                Meteor.call('addProjDetail', Session.get('itemName'), projectID, "NA", details, projDetails[0].customerID, "fa fa-trash fa-lg");
                tasksList.map( task => {
                    Meteor.call('deleteTask', task._id);
                    details = "Deleted entire project of '" + Session.get('itemName') + "'. Task deleted: " + task.taskName;
                    Meteor.call('addProjDetail', Session.get('itemName'), projectID, task._id, details, projDetails[0].customerID, "fa fa-trash fa-lg");
                })
            }
        }
    }

    deleteConfirm() {
        this.setState({submitType: "save"});
    }

    getItemDetails() {
        var deleteConfirm;
        let itemName = Session.get('itemName');
        let customerProfile = this.getCustomer();
        if (customerProfile) {
            var customerFirstName = customerProfile.firstName;
            var customerLastName = customerProfile.lastName;

            deleteConfirm = "Delete entire project '" + itemName + "' for " + customerFirstName + " " + customerLastName + "?";
        } else if (Session.get('taskID')) {
            let taskID = Session.get('taskID');
            let getTask = TasksCollections.find({_id: taskID}).fetch();
            let subID = getTask[0].subID;
            let projectID = getTask[0].projectID;
            Session.set('projectID', projectID);
            Session.set('subID', subID);
            let countTasks = TasksCollections.find({$and: [{projectID: projectID},{subID: {$regex: subID}}]}).count() - 1;
            let childLevel = getTask[0].child;

            if (countTasks > 0) {
                deleteConfirm = "WARNING! This task has " + countTasks + " child task(s). Delete task '" + itemName + "' and all its child tasks?";
            } else {
                deleteConfirm = "Please confirm. Delete task '" + itemName + "'?";
            }
        }
        return deleteConfirm;
    }

    render() {
        var deleteConfirm = this.getItemDetails();

        return (
            <div className="addTask">
                <form onSubmit={this.deleteItem.bind(this)}>
                    <div className="col-xs-12" style={{height:'85px'}}>
                        <div className="h3-modal text-center">Delete Item</div>
                        <div className="row">
                            <text className="col-xs-12">{deleteConfirm}</text>
                        </div>
                    </div>
                    <div className="col-xs-12 modal-save" style={{height:'10px'}}/>
                    <div className="col-xs-12">
                        <button className="btn btn-warning col-xs-3 text-center" type="submit" onClick={this.deleteConfirm.bind(this)}>YES</button>
                        <button className="btn btn-default col-xs-3 pull-right text-center" type="submit" onClick={this.cancelButton.bind(this)}>CANCEL</button>
                    </div>
                </form>
            </div>
        )
    }
}
