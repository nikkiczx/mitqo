import React from 'react';
import moment from "moment";
import Rodal from 'rodal';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactDOM from 'react-dom';
import Timeline from 'react-calendar-timeline';
import ReactTooltip from 'react-tooltip'

import AddChildTask from './AddChildTask.jsx';
import EditTask from './EditTask.jsx';
import DeleteModal from './DeleteModal.jsx';

export default class TasksTable extends TrackerReact(React.Component) {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            visible2: false,
            visible3: false,
            visible4: false,
            subscription: {
                VSprojects: Meteor.subscribe("listProjects"),
                allTasks: Meteor.subscribe("taskList"),
                profiles: Meteor.subscribe("listProfiles")
            }
        };
    }

    show() {
        this.setState({ visible: true });

        let projectID = this.props.task.projectID;
        Session.set('projectID', projectID);

        let parentID = this.props.task.parentID;
        Session.set('parentID', parentID);

        let subID = this.props.task.subID;
        Session.set('subID', subID);

        let childLevel = this.props.task.child;
        Session.set('childLevel', childLevel);
    }

    hide() {
        this.setState({ visible: false });
    }

    editRow() {

        let projID = this.props.task.projectID;
        let getProject = ProjectsCollections.find({_id: projID}).fetch();

        let projLockStatus = getProject[0].disp;

        if (projLockStatus === "locked") {
            if (this.props.task.user === Meteor.userId()) {
                this.setState({ visible2: true});
            } else {
                Bert.alert('Warning! Unauthorized to edit task.', 'danger', 'fixed-top');
            }
        } else {
            this.setState({ visible2: true});

            let taskName = this.props.task.taskName;
            Session.set('taskName', taskName);

            let taskID = this.props.task._id;
            Session.set('taskID', taskID);
        }


    }

    showDelete() {

        let projID = this.props.task.projectID;
        let getProject = ProjectsCollections.find({_id: projID}).fetch();

        let projLockStatus = getProject[0].disp;

        let taskName = this.props.task.taskName;
        Session.set('itemName', taskName);

        let taskID = this.props.task._id;
        Session.set('taskID', taskID);

        let customerID = this.props.task.customerID;
        Session.set('customerID', customerID);

        if (projLockStatus === "unlocked") {
            this.setState({ visible3: true });
        } else {
            if (this.props.task.user === Meteor.userId()) {
                this.setState({ visible3: true });
            } else {
                Bert.alert('Warning! Unauthorized to delete task.', 'danger', 'fixed-top');
            }
        }
    }

    hideDelete() {
        this.setState({ visible3: false});
    }

    hideCompleted() {
        this.setState({ visible4: false});
    }

    taskCompleted() {
        this.setState({ visible4: true });
    }

    saveCompleted() {
        this.setState({submitType: "save"});
    }

    cancelCompleted() {
        this.setState({submitType: "cancel"});
    }

    taskCompletedSave() {
        if (this.state.submitType === "save") {
            let updatestat = this.refs.updateDetails.value.trim();
            let nowDate =  new Date();
            let taskID = this.props.task._id;
            let taskUpdate = "Completed: " + updatestat;

            let taskDetail = TasksCollections.findOne({_id: taskID});

            if (updatestat) {
                Meteor.call('updateProgress', taskID, nowDate, "completed", taskUpdate);
                Meteor.call('addProjDetail', taskDetail.projectID, taskID, taskUpdate, "NA", "fa fa-pencil-square-o fa-lg", Meteor.userId());
            }
        }
    }

    getAssistants(userID) {
        var loadProfile = Profiles.find({assisting: Meteor.userId()}).fetch();
        var assistantDB = Profiles.findOne({user: userID});

        return assistantDB;
    }

    render() {
        var startDate = moment(this.props.task.startDate).format('MMM DD YY, h:mm A');
        var endDate = moment(this.props.task.endDate).format('MMM DD YY, h:mm A');

        var childPadding = '30px'; var sublinePadding = '50px';
        var childRow = '470px';
        if (this.props.task.child === 1){
            childPadding = '35px'; childRow = '465px'; sublinePadding = '55px';
        } else if (this.props.task.child === 2){
            childPadding = '40px'; childRow = '460px'; sublinePadding = '60px';
        } else if (this.props.task.child === 3){
            childPadding = '45px'; childRow = '455px'; sublinePadding = '66px';
        } else if (this.props.task.child === 4){
            childPadding = '50px'; childRow = '450px'; sublinePadding = '70px';
        }

        let nowDateTime = new Date();
        var taskHighlight; var checkTaskDone = "visible"; var doneHighlight;

        if (this.props.task.endDate < nowDateTime) {
            if (this.props.task.progress !== "completed") {
                if (this.props.task.assigned !== Meteor.userId()) {
                    taskHighlight = "#ff9900"
                } else {
                    taskHighlight = "#ff3333";
                }
            }
        }

        if (this.props.task.progress === "completed") {
            doneHighlight = "#00b33c";
            //checkTaskDone = "hidden";
        }

        var assistantProfile = this.getAssistants(this.props.task.assigned);
        var assistantName = "";
        var assistantInitials = "";

        if (assistantProfile) {
          if (this.props.task.assigned !== Meteor.userId()) {
              assistantInitials = assistantProfile.firstName.charAt(0) + assistantProfile.lastName.charAt(0);
              assistantName = assistantProfile.firstName + " " + assistantProfile.lastName;
          }
        }

        return (
            <div>
                <div className="col-xs-1" style={{width: childPadding}}></div>
                <div className="col-xs-1 zero-padding" style={{width:'12px', bottom: '2px'}} >
                    <i onClick={this.show.bind(this)}><img src="/images/square-plus-icon.png" width="12px"></img>
                        <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                            <AddChildTask />
                        </Rodal>
                    </i>
                </div>
                <ReactTooltip />
                <div className="row table-row-hover" data-tip={this.props.task.comment} style={{color: taskHighlight}} onClick={this.editRow.bind(this)}>
                    <Rodal visible={this.state.visible2} onClose={this.hide.bind(this)}>
                        <EditTask />
                    </Rodal>
                    <div>
                        <text className="col-xs-7 text-left row-project-padding" style={{width: childRow}}>{this.props.task.taskName.substring(0,45)}</text>
                        <i onClick={this.showDelete.bind(this)} className="col-xs-1 zero-padding" style={{width:'15px'}}>
                            <img src="/images/delete-icon.png" width="15px"></img>
                            <Rodal visible={this.state.visible3} onClose={this.hideDelete.bind(this)}>
                                <DeleteModal />
                            </Rodal>
                        </i>
                        <text className="col-xs-1 row-project-padding text-center" data-tip={assistantName} style={{width:'25px', left:'7px'}}>{assistantInitials}</text>
                        <i onClick={this.taskCompleted.bind(this)} className="col-xs-1 zero-padding fa fa-check-square" style={{width:'15px', top:'3px', right:'-20px', visibility: checkTaskDone, color: doneHighlight}}>
                            <Rodal visible={this.state.visible4} onClose={this.hideCompleted.bind(this)}>
                              <div className="col-xs-12 editProj table-fonts">
                                <form onSubmit={this.taskCompletedSave.bind(this)}>
                                    <div className="col-xs-2 h3-modal pull-left">Update</div>
                                    <div className="col-xs-7 zero-padding" style={{top:'10px'}}>Task: {this.props.task.taskName}</div>
                                    <div className="col-xs-3 zero-padding" style={{top:'10px'}}>Due: {moment(this.props.task.endDate).format("DD MMM YYYY HH:mm A")}</div>
                                    <div><input className="col-xs-12" style={{top:'10px'}} ref="updateDetails" placeholder="Please enter update information to close item" type="text"></input></div>
                                    <div className="col-xs-12 modal-save" style={{height:'10px', top:'5px'}}/>
                                    <div className="col-xs-12" style={{top:'10px'}}>
                                        <button className="btn btn-warning col-xs-3 text-center" type="submit" onClick={this.saveCompleted.bind(this)}>SAVE</button>
                                        <button className="btn btn-default col-xs-3 pull-right text-center" type="submit" onClick={this.cancelCompleted.bind(this)}>CANCEL</button>
                                    </div>
                                </form>
                              </div>
                            </Rodal>
                        </i>
                    </div>
                </div>

            </div>
        )
    }
}
