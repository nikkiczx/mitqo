import React, {Component} from 'react';
import moment from "moment";
import Rodal from 'rodal';
import ReactiveVar from 'meteor/reactive-var';

export default class NotificationsUpdate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            submitType: "cancel"
        };
    }

    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }

    cancelButton() {
        this.setState({submitType: "cancel"});
    }

    saveButton() {
        this.setState({submitType: "save"});
    }

    taskCompleted() {
        if (this.state.submitType === "save") {
            let updatestat = this.refs.updateDetails.value.trim();
            let nowDate =  new Date();
            let taskID = this.props.task._id;

            if (updatestat) {
                Meteor.call('updateProgress', taskID, nowDate, "completed");
            }
        }
    }

    render() {
        return (
              <div>
                <div className="col-xs-11 zero-padding">Task: {this.props.task.taskName}</div>
                <div className="col-xs-1 zero-padding"><i className="fa fa-check-square-o" onClick={this.show.bind(this)}>
                  <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                    <div className="col-xs-12 editProj table-fonts">
                      <form onSubmit={this.taskCompleted.bind(this)}>
                          <div className="col-xs-2 h3-modal pull-left">Update</div>
                          <div className="col-xs-7 zero-padding" style={{top:'10px'}}>Task: {this.props.task.taskName}</div>
                          <div className="col-xs-3 zero-padding" style={{top:'10px'}}>Due: {moment(this.props.task.endDate).format("DD MMM YYYY HH:mm A")}</div>
                          <div><input className="col-xs-12" style={{top:'10px'}} ref="updateDetails" placeholder="Please enter update information to close item" type="text"></input></div>
                          <div className="col-xs-12 modal-save" style={{height:'10px', top:'5px'}}/>
                          <div className="col-xs-12" style={{top:'10px'}}>
                              <button className="btn btn-warning col-xs-3 text-center" type="submit" onClick={this.saveButton.bind(this)}>SAVE</button>
                              <button className="btn btn-default col-xs-3 pull-right text-center" type="submit" onClick={this.cancelButton.bind(this)}>CANCEL</button>
                          </div>
                      </form>
                    </div>
                  </Rodal>
                </i></div>
                <div>Due: {moment(this.props.task.endDate).format("DD MMM YYYY HH:mm A")}</div>
              </div>

            )
        }
    }
