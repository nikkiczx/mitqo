import React from 'react';
import AccountsUI from '../AccountsUI.jsx';

export const PublicLayout = ({content}) => (
  <div>
      <div className="navbar navbar-black navbar-fixed-top">
          <div>
              <a href="/"><img src="/images/mitqo.png" width="60"></img></a>
              <a href="/profile" className="pull-right-config"><img src="/images/settings-cogwheel.png" width="30"></img></a>
              <a className="navbar-header"><AccountsUI /></a>
          </div>
      </div>
      <main>
        <div className="zero-padding">
            {content}
        </div>
    </main>
  </div>
)
