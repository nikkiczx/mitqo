import React from 'react';
import AccountsUI from '../AccountsUI.jsx';
import SplitterLayout from 'react-splitter-layout';

import Notifications from '../pm/Notifications.jsx';

export const MainLayout = ({content}) => (
  <div>
      <div className="navbar navbar-black navbar-fixed-top">
          <div>
              <a href="/"><img src="/images/mitqo.png" width="60"></img></a>
              <a href="/profile" className="pull-right-config"><img src="/images/settings-cogwheel.png" width="30"></img></a>
              <a className="navbar-header"><AccountsUI /></a>
          </div>
      </div>
      <main>
          <SplitterLayout secondaryInitialSize={250}>
              <div className="zero-padding">
                  {content}
              </div>
              <div className="zero-padding">
                  <Notifications />
              </div>
          </SplitterLayout>
    </main>
  </div>
)
