import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class AddAssistant extends TrackerReact(React.Component) {

    constructor(){
        super();

        this.state = {
            submitType: "cancel",
            subscription: {
                myProfile: Meteor.subscribe("listProfiles"),
                allCustomers: Meteor.subscribe("listCustomers")
            }
        }
    }

    componentWillUnmount() {
        this.state.subscription.allCustomers.stop();
        this.state.subscription.myProfile.stop();
    }

    addAssistant(e) {
        let assistantEmail = this.refs.email.value.trim();
        if (assistantEmail) {
            if (this.state.submitType === "save") {
                let findAssistant = Profiles.findOne({email: assistantEmail});
                let assistantStatus = findAssistant.email;

                if (assistantStatus) {
                    let allAssisting = findAssistant.assisting;
                    var checkExist = allAssisting.indexOf(Meteor.userId()) > -1;

                    if (checkExist === false) {
                        Meteor.call('updateAssistant', Meteor.userId(), findAssistant._id);
                    } else {
                        e.preventDefault();
                        Bert.alert('This assistant has already been added.', 'danger', 'fixed-top');
                    }
                } else {
                    //Email onboarding
                }
            }
        }
    }

    cancelButton() {
        this.setState({submitType: "cancel"});
    }

    saveButton() {
        this.setState({submitType: "save"});
    }

    render() {
        return (
            <div className="editProj">
                <form onSubmit={this.addAssistant.bind(this)}>
                    <div className="col-xs-12" style={{height:'120px'}}>
                        <div className="h3-modal text-center">Add New Assistant</div>
                        <div className="row">
                            <div className="col-xs-12">
                                <input ref="email" type="text" placeholder="Email Address" className="form-control" />
                            </div>
                            <div className="col-lg-12 table-fonts">
                                NOTE: Only validated users who are registered on MITQO can be added.
                            </div>
                        </div>
                    </div>
                    <div className="row col-xs-12">

                    </div>
                    <div className="col-xs-12 modal-save" style={{height:'10px'}}/>
                    <div className="row"/>
                    <div className="col-xs-12">
                        <button className="btn btn-warning col-xs-3 text-center" type="submit" onClick={this.saveButton.bind(this)}>ADD</button>
                        <button className="btn btn-default col-xs-3 pull-right text-center" type="submit" onClick={this.cancelButton.bind(this)}>CANCEL</button>
                    </div>
                </form>
            </div>
        )
    }
}
