import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class AddCustomer extends TrackerReact(React.Component) {

    constructor(){
        super();

        this.state = {
            submitType: "cancel",
            subscription: {
                myProfile: Meteor.subscribe("listProfiles"),
                allCustomers: Meteor.subscribe("listCustomers")
            }
        }
    }

    componentWillUnmount() {
        this.state.subscription.allCustomers.stop();
        this.state.subscription.myProfile.stop();
    }

    getCustomers() {
        return Customers.find().fetch();
    }

    handleChange(e) {
        e.preventDefault();
        this.state.value = e.target.value;
        let fromCustomer = Customers.findOne({_id: this.state.value});
        let customerName = fromCustomer.customer;
        Session.set("CN", customerName);
    }

    addCustomer() {
        if (this.state.submitType === "save") {
            Meteor.call('newCustomers', this.refs.first_name.value.trim(), this.refs.last_name.value.trim(), this.refs.email.value.trim(), "");
        }
    }

    cancelButton() {
        this.setState({submitType: "cancel"});
    }

    saveButton() {
        this.setState({submitType: "save"});
    }

    render() {
        return (
            <div className="editProj">
                <form onSubmit={this.addCustomer.bind(this)}>
                    <div className="col-xs-12" style={{height:'120px'}}>
                        <div className="h3-modal text-center">Add New Customer</div>
                        <div className="row">
                            <div className="col-xs-6">
                                <input ref="first_name" type="text" placeholder="First Name" className="form-control"/>
                            </div>
                            <div className="col-xs-6 zero-padding">
                                <input ref="last_name" type="text" placeholder="Last Name" className="form-control"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12">
                                <input ref="email" type="text" placeholder="Email Address" className="form-control" />
                            </div>
                        </div>
                    </div>
                    <div className="row col-xs-12">

                    </div>
                    <div className="col-xs-12 modal-save" style={{height:'10px'}}/>
                    <div className="row"/>
                    <div className="col-xs-12">
                        <button className="btn btn-warning col-xs-3 text-center" type="submit" onClick={this.saveButton.bind(this)}>ADD</button>
                        <button className="btn btn-default col-xs-3 pull-right text-center" type="submit" onClick={this.cancelButton.bind(this)}>CANCEL</button>
                    </div>
                </form>
            </div>
        )
    }
}
