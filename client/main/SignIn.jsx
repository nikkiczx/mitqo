import React, {Component} from 'react';

Meteor.call('Accounts.config', {loginExpirationInDays: 1});

export default class SignIn extends Component {
    render() {
        return (
            <div className="col-xs-12 text-center" style={{top:'100px'}}>
                <h2>Please sign in to continue...</h2>
            </div>
            )
        }
    }
