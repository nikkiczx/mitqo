import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

import UpdateHistory from './UpdateHistory.jsx';

Meteor.call('Accounts.config', {loginExpirationInDays: 1});

export default class MainPage extends TrackerReact(React.Component) {
    render() {
        const showButton = Meteor.userId() !== null ? <button><a href="/projects" className="btn">Go To Projects</a></button> : '';
        const showHistory = Meteor.userId() !== null ? <UpdateHistory /> : '';
        return (
            <div className="col-xs-12">
                <h2 className="text-center" id="projectsTitle">Welcome to Mitqo</h2>
                <div className="text-center table-fonts">Closed Beta Testing in Progress</div>
                <div className="col-xs-12 center" style={{top:'100px'}}>{showButton}</div>
                {/*<div className="col-xs-12 center" style={{top:'50px'}}><button onClick={this.sendEmail.bind(this)}>Send Email</button></div>*/}
                <div className="col-xs-12" style={{top:'100px'}}>
                <div className="col-xs-12 center table-fonts">If you are interested to join our closed beta testing, email us at new.miqto@gmail.com.</div>
                <div className="row">{showHistory}</div>
                </div>
            </div>
            )
        }
    }
