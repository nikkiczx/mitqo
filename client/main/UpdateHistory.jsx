import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class UpdateHistory extends TrackerReact(React.Component) {
    render() {
        return (
            <div className="col-xs-12">
                <h2 id="projectsTitle" className="officialColor">Software Update History</h2>
                <div className="col-xs-12 table-fonts">
                    <h3 className="col-xs-1">version</h3>
                    <h3 className="col-xs-2">date</h3>
                    <h3 className="col-xs-9">details</h3>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v42</div>
                    <div className="col-xs-2">2 Jul 2017</div>
                    <div className="col-xs-9">
                        <div>1. Fixed child task table layout error</div>
                        <div>2. Added ProjectDetails page</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v41</div>
                    <div className="col-xs-2">29 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>Fixed MAIL-URL. Enabled forgotten password emailing.</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v40</div>
                    <div className="col-xs-2">22 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>Fixed typo in v39</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v39</div>
                    <div className="col-xs-2">21 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>Added assistant remove function</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v38</div>
                    <div className="col-xs-2">14 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>1. Added assisting request/approve function</div>
                        <div>2. Added assisting terminate function</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v37</div>
                    <div className="col-xs-2">10 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>1. Changed overdue highlight from entire row to only fonts and icons</div>
                        <div>2. Added notifications for overdue and due soon items for assigned tasks</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v36</div>
                    <div className="col-xs-2">6 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>Fixed multiple admin view - Did not show tasks</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v35</div>
                    <div className="col-xs-2">4 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>1. Added Software Update History on MainPage</div>
                        <div>2. Fixed ProfilePage where Assistants did not show up - status not changed to active</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v34</div>
                    <div className="col-xs-2">3 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>1. Fixed AddChildTask error - could not add child task</div>
                        <div>2. Fixed DeleteTask error - was counting child tasks wrongly</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v33</div>
                    <div className="col-xs-2">2 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>Added multiple admin view</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v32</div>
                    <div className="col-xs-2">1 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>1. Removed done icon from row after ask is updated</div>
                        <div>2. Added short note at the bottom of AddAssistant to say that only validated users registered on mitqo can be added as assistants</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v31</div>
                    <div className="col-xs-2">1 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>Added tooltip for completed task comment</div>
                    </div>
                </div>
                <div className="col-xs-12 table-fonts">
                    <div className="col-xs-1">v30</div>
                    <div className="col-xs-2">1 Jun 2017</div>
                    <div className="col-xs-9">
                        <div>1. Added tooltip for customer & assistant names</div>
                        <div>2. Corrected calendar layout error</div>
                        <div>3. AddAssistant - allow to add users already on the system as assistants witih their email</div>
                    </div>
                </div>
            </div>
            )
        }
    }
