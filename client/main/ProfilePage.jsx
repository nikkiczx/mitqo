import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import Rodal from 'rodal';

import AddCustomer from './AddCustomer.jsx';
import AddAssistant from './AddAssistant.jsx';

Customers = new Mongo.Collection("customers");
Profiles = new Mongo.Collection("profiles");

export default class ProfilePage extends TrackerReact(React.Component) {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            visible2: false,
            subscription: {
                myProfile: Meteor.subscribe("listProfiles"),
                allCustomers: Meteor.subscribe("listCustomers")
            }
        };
    }

    componentWillUnmount() {
        this.state.subscription.myProfile.stop();
        this.state.subscription.allCustomers.stop();
    }

    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }

    showAssistant() {
        this.setState({ visible2: true });
    }

    hideAssistant() {
        this.setState({ visible2: false });
    }

    submitForm(event) {
        event.preventDefault();

        var firstName; var lastName;
        let profile = Profiles.find({user: Meteor.userId()}).fetch();

        if(profile[0]) {
            let profileID = profile[0]._id;
            firstName = profile[0].firstName;
            lastName = profile[0].lastName;

            if (this.refs.first_name.value.trim()) {
                firstName = this.refs.first_name.value.trim();
            }

            if (this.refs.last_name.value.trim()) {
                lastName = this.refs.last_name.value.trim();
            }

            Meteor.call('updateProfile', profileID, firstName, lastName);
        } else {
            let getUser = Meteor.users.find().fetch();
            let emailArr = getUser[0].emails;
            let email = emailArr[0].address;

            if (this.refs.first_name.value.trim()) {
                firstName = this.refs.first_name.value.trim();
            }

            if (this.refs.last_name.value.trim()) {
                lastName = this.refs.last_name.value.trim();
            }

            Meteor.call('newProfile', firstName, lastName, email);
        }


        Bert.alert({
          hideDelay: 100,
          message: 'Profile Update Succesful',
          type: 'success',
          style: 'fixed-top',
          icon: 'fa-check'
        });

        this.refs.first_name.value = "";
        this.refs.last_name.value = "";
    }

    saveCustomerDetails(e) {
        e.preventDefault();

        var firstName = this.refs.customer_first_name.value.trim();
        var lastName = this.refs.customer_last_name.value.trim();

        //Meteor.call('updateCustomers', firstName, lastName, "");

        Bert.alert({
          hideDelay: 100,
          message: 'Customer Profile Update Succesful',
          type: 'success',
          style: 'fixed-top',
          icon: 'fa-check'
        });

        this.refs.customer_first_name.value = "";
        this.refs.customer_last_name.value = "";

    }

    getFirstName() {
        var profile = Profiles.find({user: Meteor.userId()}).fetch(); var FirstName;

        if (profile[0]) {
            FirstName = profile[0].firstName;
        } else {
            FirstName = "Please update your profile";
        }

        return FirstName;
    }

    getLastName() {
        var profile = Profiles.find({user: Meteor.userId()}).fetch(); var LastName;
        if (profile[0]) {
            LastName = profile[0].lastName;
        } else {
            LastName = "Please update your profile";
        }

        return LastName;
    }

    getEmail() {
        var profile = Profiles.find({user: Meteor.userId()}).fetch(); var Email;
        if (profile[0]) {
            Email = profile[0].email;
        }
        return Email;
    }

    getCustomers() {
        return Customers.find({$or: [{user: Meteor.userId()}, {assigned: Meteor.userId()}]}).fetch();
    }

    getAssistants() {
        let assistantList = Profiles.find({$and: [{assisting: Meteor.userId()}, {status:"active"}]}).fetch();

        return assistantList;
    }

    getAssisting() {
        let results = [];
        let assistantList = Profiles.find({$and: [{assistedBy: Meteor.userId()}, {status:"active"}]}).fetch();
        assistantList.map(profs => {
            reqAcct = profs.user;
            let findAsst = Profiles.find({$and: [{user: Meteor.userId()}, {assisting: {$eq: reqAcct}}]}).fetch();
            if (findAsst.length > 0 ) {
                results.push(profs);
            }
        });

        return results;
    }

    getAssistRequest() {
        let results = [];
        let assistantList = Profiles.find({$and: [{assistedBy: Meteor.userId()}, {status:"active"}]}).fetch();
        assistantList.map(profs => {
            reqAcct = profs.user;
            let findAsst = Profiles.find({$and: [{user: Meteor.userId()}, {assisting: {$ne: reqAcct}}]}).fetch();
            if (findAsst.length > 0 ) {
                results.push(profs);
            }
        });
        return results;
    }

    approveAssist(reqID) {
        let requestID = reqID;
        Meteor.call('updateAssisting', requestID);
        Bert.alert('Assisting request approved!', 'success', 'fixed-top');
    }

    removeAssist(reqID) {
        let requestID = reqID;
        Meteor.call('removeAssisting', requestID);
        Meteor.call('removeAssistant', requestID);
        Bert.alert('Assisting terminated.', 'danger', 'fixed-top');
    }

    render() {
        let emailPlaceholder = this.getEmail();
        let firstNamePlaceholder = this.getFirstName();
        let lastNamePlaceholder = this.getLastName();

        return (
            <div>
                <div className="col-xs-12">
                    <div className="col-xs-6 center">
                    <h2 id="projectsTitle">My Profile</h2></div>
                    <form onSubmit={this.submitForm.bind(this)}>
                        <div className="col-xs-12 table-fonts center">
                            <div className="row col">
                                <div className="form-group">
                                    <label className="col-xs-3">First Name: </label>
                                    <input className="col-xs-3" ref="first_name" placeholder= { firstNamePlaceholder }></input>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group">
                                    <label className="col-xs-3">Last Name:</label>
                                    <input className="col-xs-3" ref="last_name" placeholder= { lastNamePlaceholder }></input>
                                </div>
                            </div>
                            <div className="row">
                                <div classname="form-group">
                                    <label className="col-xs-3">Email</label>
                                    <label className="col-xs-3">{ emailPlaceholder }</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group text-center">
                                    <button>Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div className="col-xs-12" style={{top:'100px'}}>
                  <div className="col-xs-6 text-right">
                    <h2 id="projectsTitle">Customer Profiles</h2>
                    </div>
                    <div className="col-xs-6" style={{left: '-20px', top:'25px'}}>
                        <i onClick={this.show.bind(this)}>
                            <img src="/images/plus_icon.png" width="60px"></img>
                            <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                                <AddCustomer />
                            </Rodal>
                        </i>
                    </div>
                    <div className="col-xs-9 table-fonts center">
                        <h3 className="col-xs-4">First Name </h3>
                        <h3 className="col-xs-4">Last Name</h3>
                        <h3 className="col-xs-4">Email</h3>
                    </div>
                    {this.getCustomers().map( customer => {
                        return (
                            <form onSubmit={this.saveCustomerDetails.bind(this)} key={customer._id} customer={customer}>
                            <div className="col-xs-12 table-fonts center">
                                <div className="col-xs-9">
                                    <div className="form-group col-xs-4">
                                        <label>{ customer.firstName }</label>
                                    </div>
                                    <div className="form-group col-xs-4">
                                        <label>{ customer.lastName }</label>
                                    </div>
                                    <div className="form-group col-xs-4">
                                        <label>{ customer.email }</label>
                                    </div>
                                    <div className="row" style={{height: '100px'}}></div>
                                </div>
                                <div className="col-xs-3"> Notes
                                    <div className="row">
                                        <div className="form-group text-center">
                                            <button>Add Notes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        )
                    })}
                </div>

                <div className="col-xs-12" style={{top:'100px'}}>
                  <div className="col-xs-6 text-right">
                    <h2 id="projectsTitle">Assistants</h2>
                    </div>
                    <div className="col-xs-6" style={{left: '-20px', top:'25px'}}>
                        <i onClick={this.showAssistant.bind(this)}>
                            <img src="/images/plus_icon.png" width="60px"></img>
                            <Rodal visible={this.state.visible2} onClose={this.hideAssistant.bind(this)}>
                                <AddAssistant />
                            </Rodal>
                        </i>
                    </div>
                    <div className="col-xs-9 table-fonts center">
                        <h3 className="col-xs-4">First Name </h3>
                        <h3 className="col-xs-4">Last Name</h3>
                        <h3 className="col-xs-4">Email</h3>
                    </div>
                    {this.getAssistants().map( assistant => {
                        return (
                            <form onSubmit={this.saveCustomerDetails.bind(this)} key={assistant._id} assistant={assistant}>
                            <div className="col-xs-12 table-fonts center">
                                <div className="col-xs-9">
                                    <div className="form-group col-xs-4">
                                        <label>{ assistant.firstName }</label>
                                    </div>
                                    <div className="form-group col-xs-4">
                                        <label>{ assistant.lastName }</label>
                                    </div>
                                    <div className="form-group col-xs-4">
                                        <label>{ assistant.email }</label>
                                    </div>
                                    <div className="row" style={{height: '100px'}}></div>
                                </div>
                                <div className="col-xs-3">
                                  <div className="form-group text-center">
                                      <button value={assistant.user} onClick={this.removeAssist.bind(this,assistant.user)} style={{color:'blue'}}>Remove</button>
                                  </div>
                                </div>
                            </div>
                            </form>
                        )
                    })}
                </div>

                <div className="col-xs-12" style={{top: '100px'}}>
                  <div className="col-xs-6 text-right">
                    <h2 id="projectsTitle">Assisting</h2>
                  </div>
                  <div className="col-xs-9 table-fonts center">
                      <h3 className="col-xs-4">First Name </h3>
                      <h3 className="col-xs-4">Last Name</h3>
                      <h3 className="col-xs-4">Email</h3>
                  </div>
                  {this.getAssisting().map( assistant => {
                      return (
                          <form onSubmit={this.saveCustomerDetails.bind(this)} key={assistant._id} assistant={assistant}>
                          <div className="col-xs-12 table-fonts center">
                              <div className="col-xs-9">
                                  <div className="form-group col-xs-4">
                                      <label>{ assistant.firstName }</label>
                                  </div>
                                  <div className="form-group col-xs-4">
                                      <label>{ assistant.lastName }</label>
                                  </div>
                                  <div className="form-group col-xs-4">
                                      <label>{ assistant.email }</label>
                                  </div>
                                  <div className="row" style={{height: '100px'}}></div>
                              </div>
                              <div className="col-xs-3">
                                <div className="form-group text-center">
                                    <button value={assistant.user} onClick={this.removeAssist.bind(this, assistant.user)} style={{color: 'blue'}}>Remove</button>
                                </div>
                              </div>
                          </div>
                          </form>
                      )
                  })}
                  {this.getAssistRequest().map( request => {
                      return (
                        <div className="col-xs-12 table-fonts center" key={request._id} request={request}>
                            <div className="col-xs-9">
                                <div className="form-group col-xs-4">
                                    <label>{ request.firstName }</label>
                                </div>
                                <div className="form-group col-xs-4">
                                    <label>{ request.lastName }</label>
                                </div>
                                <div className="form-group col-xs-4">
                                    <label>{ request.email }</label>
                                </div>
                                <div className="row" style={{height: '100px'}}></div>
                            </div>
                            <div className="col-xs-3">
                              <div className="form-group text-center">
                                  <button value={request.user} onClick={this.approveAssist.bind(this, request.user)} style={{color: 'green'}}>Approve</button>
                              </div>
                            </div>
                        </div>
                      )
                  })}

                </div>
            </div>
        )
    }
}
